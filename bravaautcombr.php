<?php
/**
 * Brava Automação - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Brava
 * @author    Cimbre <contato@cimbre.com.br>
 * @copyright 2020 Cimbre
 * @license   Proprietary https://cimbre.com.br
 * @link      https://bravaaut.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Brava Automação - mu-plugin
 * Plugin URI:  https://bravaaut.com.br
 * Description: Customizations for bravaaut.com.br site
 * Version:     1.0.1
 * Author:      Cimbre
 * Author URI:  https://cimbre.com.br/
 * Text Domain: bravaaut
 * License:     Proprietary
 * License URI: https://cimbre.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('bravaautcombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_bravaautcombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_hero_id',
                'title'         => __('Hero', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'bravaautcombr'),
                    'add_button'   =>__('Add Another Slide', 'bravaautcombr'),
                    'remove_button' =>__('Remove Slide', 'bravaautcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'bravaautcombr'),
                'description' => '',
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'bravaautcombr'),
                'description' => '',
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
         * About
         ******/
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_about_id',
                'title'         => __('About', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //About Background Color
        $cmb_about->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //About Caption
        $cmb_about->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_caption',
                'type'       => 'text_medium',
            )
        );

        //About Title
        $cmb_about->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'textarea_small',
            )
        );

        //About Text
        $cmb_about->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_text',
                'type'       => 'textarea_code',
            )
        );

        //About Button Text
        $cmb_about->add_field(
            array(
                'name'       => __('Button Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_btn_text',
                'type'       => 'text_medium',
            )
        );

        //About Button URL
        $cmb_about->add_field(
            array(
                'name'       => __('Button URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'about_btn_url',
                'type'       => 'text_medium',
            )
        );
        
        /**
         * Services
         */
        $cmb_services = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_services_id',
                'title'         => __('Services', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Services Background Color
        $cmb_services->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );

        //Services Image
        $cmb_services->add_field(
            array(
                'name'        => __('Image', 'bravaautcombr'),
                'description' => '',
                'id'          => $prefix . 'services_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Services Title
        $cmb_services->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_title',
                'type'       => 'textarea_small',
            )
        );

        //Services Text
        $cmb_services->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_text',
                'type'       => 'textarea_code',
            )
        );

        //Services Link Text
        $cmb_services->add_field(
            array(
                'name'       => __('Link Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'services_link_text',
                'type'       => 'text_medium',
            )
        );

        //Services Link URL
        $cmb_services->add_field(
            array(
                'name'       => __('Link URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'services_link_url',
                'type'       => 'text_medium',
            )
        );

        /**
         * Projects
         */
        $cmb_projects = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_projects_id',
                'title'         => __('Projects', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Projects Background Color
        $cmb_projects->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'projects_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Projects Caption
        $cmb_projects->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'projects_caption',
                'type'       => 'text_medium',
            )
        );

        //Projects Title
        $cmb_projects->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'projects_title',
                'type'       => 'textarea_small',
            )
        );

        /**
         * Products
         */
        $cmb_products = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_products_id',
                'title'         => __('Products', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Products Background Color
        $cmb_products->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'products_bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Product Image
        $cmb_products->add_field(
            array(
                'name'        => __('Image', 'bravaautcombr'),
                'description' => '',
                'id'          => $prefix . 'products_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Products Title
        $cmb_products->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'products_title',
                'type'       => 'textarea_small',
            )
        );

        //Products Text
        $cmb_products->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'products_text',
                'type'       => 'textarea_code',
            )
        );

        //Products Link Text
        $cmb_products->add_field(
            array(
                'name'       => __('Link Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'products_link_text',
                'type'       => 'text_medium',
            )
        );

        //Products Link URL
        $cmb_products->add_field(
            array(
                'name'       => __('Link URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'products_link_url',
                'type'       => 'text_medium',
            )
        );

        /******
         * Contact
         ******/
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_frontpage_contact_id',
                'title'         => __('Contact', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contact Background Color
        $cmb_contact->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Contact Caption
        $cmb_contact->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_caption',
                'type'       => 'text_medium',
            )
        );

        //Contact Title
        $cmb_contact->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'textarea_small',
            )
        );

        //Contact Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_text',
                'type'       => 'textarea_code',
            )
        );

        //Contact Button Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Button Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Contact Button URL
        $cmb_contact->add_field(
            array(
                'name'       => __('Button URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'contact_btn_url',
                'type'       => 'text_medium',
            )
        );
    }
);

/**
 * About
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_bravaautcombr_about_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_hero_id',
                'title'         => __('Hero', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'empresa'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'bravaautcombr'),
                    'add_button'   =>__('Add Another Slide', 'bravaautcombr'),
                    'remove_button' =>__('Remove Slide', 'bravaautcombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'bravaautcombr'),
                'description' => '',
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'bravaautcombr'),
                'description' => '',
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        /******
        * Highlight
        ******/
        $cmb_highlight = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_highlight_id',
                'title'         => __('Highlight', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Highlight Background Color
        $cmb_highlight->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'highlight_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Highlight Title
        $cmb_highlight->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'highlight_title',
                'type'       => 'textarea_small',
            )
        );

        //Highlight Subtitle
        $cmb_highlight->add_field(
            array(
                'name'       => __('Subtitle', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'highlight_subtitle',
                'type'       => 'textarea_small',
            )
        );

        //Highlight Text
        $cmb_highlight->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'highlight_text',
                'type'       => 'textarea_code',
            )
        );

        /******
        * About
        ******/
        $cmb_about = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_about_id',
                'title'         => __('About', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //About Background Color
        $cmb_about->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //About Image
        $cmb_about->add_field(
            array(
                'name'        => __('Image', 'bravaautcombr'),
                'description' => '',
                'id'          => $prefix . 'about_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'bravaautcombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //About Title
        $cmb_about->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_title',
                'type'       => 'textarea_small',
            )
        );

        //About Text
        $cmb_about->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'about_text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * History
         ******/
        $cmb_history = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_history_id',
                'title'         => __('History', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //History Background Color
        $cmb_history->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //History Caption
        $cmb_history->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_caption',
                'type'       => 'text_medium',
            )
        );

        //History Title
        $cmb_history->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_title',
                'type'       => 'textarea_small',
            )
        );

        //History Text
        $cmb_history->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_text',
                'type'       => 'textarea_code',
            )
        );

        //History Button Text
        $cmb_history->add_field(
            array(
                'name'       => __('Button Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'history_btn_text',
                'type'       => 'text_medium',
            )
        );

        //History Button URL
        $cmb_history->add_field(
            array(
                'name'       => __('Button URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'history_btn_url',
                'type'       => 'text_medium',
            )
        );
        
        /******
         * Mission
         ******/
        $cmb_mission = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_mission_id',
                'title'         => __('Mission', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Mission Background Color
        $cmb_mission->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'mission_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Mission Text
        $cmb_mission->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'mission_text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Segments
         ******/
        $cmb_segments = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_segments_id',
                'title'         => __('Segments', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Segments Background Color
        $cmb_segments->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'segments_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Segments Caption
        $cmb_segments->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'segments_caption',
                'type'       => 'text_medium',
            )
        );

        //Segments Title
        $cmb_segments->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'segments_title',
                'type'       => 'textarea_small',
            )
        );

        //Segments Text
        $cmb_segments->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'segments_text',
                'type'       => 'textarea_code',
            )
        );

        //Segments Button Text
        $cmb_segments->add_field(
            array(
                'name'       => __('Button Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'segments_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Segments Button URL
        $cmb_segments->add_field(
            array(
                'name'       => __('Button URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'segments_btn_url',
                'type'       => 'text_medium',
            )
        );

        /******
         * Contact
         ******/
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => 'bravaautcombr_about_contact_id',
                'title'         => __('Contact', 'bravaautcombr'),
                'object_types'  => array('page'), // post type
                //'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'show_on' => array( 'key' => 'page-template', 'value' => 'views/about.blade.php' ),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Contact Background Color
        $cmb_contact->add_field(
            array(
                'name'       => __('Background Color', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_bkg_color',
                'type'       => 'colorpicker',
                'default'    => 'transparent',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#f2f0f0', '#eceff1', '#f9f9f9'),
                        )
                    ),
                ),
            )
        );
        
        //Contact Caption
        $cmb_contact->add_field(
            array(
                'name'       => __('Caption', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_caption',
                'type'       => 'text_medium',
            )
        );

        //Contact Title
        $cmb_contact->add_field(
            array(
                'name'       => __('Title', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_title',
                'type'       => 'textarea_small',
            )
        );

        //Contact Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_text',
                'type'       => 'textarea_code',
            )
        );

        //Contact Button Text
        $cmb_contact->add_field(
            array(
                'name'       => __('Button Text', 'bravaautcombr'),
                'desc'       => '',
                'id'         => $prefix . 'contact_btn_text',
                'type'       => 'text_medium',
            )
        );

        //Contact Button URL
        $cmb_contact->add_field(
            array(
                'name'       => __('Button URL', 'bravaautcombr'),
                'desc'       => 'bravaaut.com.br/+',
                'id'         => $prefix . 'contact_btn_url',
                'type'       => 'text_medium',
            )
        );
    }
);
